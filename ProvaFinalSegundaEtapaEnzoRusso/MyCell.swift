//
//  MyCell.swift
//  ProvaFinalSegundaEtapaEnzoRusso
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

class MyCell: UITableViewCell {
    @IBOutlet weak var imagemPersonagem: UIImageView!
    @IBOutlet weak var namePersonagem: UILabel!
    @IBOutlet weak var nameActor: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
